<?php
require_once 'check.php';
$selectedAnswers = array();


if ($_SERVER['REQUEST_METHOD'] === "POST") {
    if (isset($_POST['nextPage'])) {
        if (isset($_COOKIE['page'])) {
            setcookie('page', $_COOKIE['page'] + 1);
            $_COOKIE['page'] = $_COOKIE['page'] + 1;
            if (isset($_COOKIE['answer'])) {
                $selectedAnswers = json_decode($_COOKIE['answer'], true);
                $selectedAnswers = $_POST + $selectedAnswers;
                setcookie('answer', json_encode($selectedAnswers));
            } else {
                setcookie('answer', json_encode($_POST));
            }
        } else {
            setcookie('page', 1);
            $_COOKIE['page'] = 1;
        }
    }
    if (isset($_POST['prev'])) {
        if (isset($_COOKIE['page'])) {
            setcookie('page', $_COOKIE['page'] - 1);
            $_COOKIE['page'] = $_COOKIE['page'] - 1;
            $selectedAnswers = json_decode($_COOKIE['answer'], true);
            $selectedAnswers = $_POST + $selectedAnswers;
            setcookie('answer', json_encode($selectedAnswers));
        } else {
            setcookie('page', 1);
            $_COOKIE['page'] = 1;
        }
    }
}
if (!isset($_COOKIE["page"]) || $_COOKIE['page'] <= 0) {
    setcookie('page', 1);
    $_COOKIE['page'] = 1;
}
function isNextPage($array, $end)
{
    return true ? $end < count($array) : false;
}
function isChoiceUser($selectedAnswers, $keyQuestion, $answer)
{
    if (isset($selectedAnswers[$keyQuestion]) && $selectedAnswers[$keyQuestion] == $answer) {
        return true;
    }
    return false;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Questions</title>
    <link rel= 'stylesheet' href = '../day09/questions.css'>
    <script type="text/js" src="../day09/questions.js"></script>
</head>

<body data-theme="light">
    <button class='theme'>Dark</button>
    <form method="POST" action="submit.php">
        <div class="questions">
            <div class="wrap-question">
                <?php $questionNum = $_COOKIE['page'] * LIMIT - LIMIT;
                foreach (array_slice(QUESTIONS, ($_COOKIE['page'] - 1) * LIMIT, LIMIT, true) as $keyQuestion => $question) :
                    $questionNum = $questionNum  + 1;
                ?>
                <div class="question">
                    <h3><span>Question <?= $questionNum ?>:</span> <?= $question ?>
                    </h3>
                    <div class="answer">
                        <?php foreach (ANSWERS[$keyQuestion]['answers'] as $answer) : ?>
                        <input type="radio" id="<?= $answer . $question ?>" name="<?= $keyQuestion ?>"
                            value="<?= $answer ?>"
                            <?php if (isChoiceUser($selectedAnswers, $keyQuestion, $answer)) echo 'checked'; ?>>
                        <label for="<?= $answer . $question ?>"> <?= $answer ?> </label><br>
                        <?php endforeach; ?>
                    </div>
                </div>
                <?php endforeach; ?>


            </div>
            <div class="action">
                <?php
                if (isNextPage(QUESTIONS, $questionNum)) {
                    echo '<input type="submit" name="nextPage" value="Next" formaction="" formmethod="POST">';
                } else {
                    echo '<input type="submit" name="prev" value="Previous" formaction="" formmethod="POST" >';
                    echo '<input type="submit" name="submit" value="Submit">';
                }
                ?>

            </div>
        </div>
    </form>
</body>

</html>
