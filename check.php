<?php
define("QUESTIONS", array(
    '0' => '1 x 0 = ?', '1' => '1 x 1 = ?',
    '2' => '1 x 2 = ?', '3' => '1 x 3 = ?',
    '4' => '1 x 4 = ?', '5' => '1 x 5 = ?',
    '6' => '1 x 6 = ?', '7' => '1 x 7 = ?',
    '8' => '1 x 8 = ?', '9' => '1 x 9 = ?',
));

define("ANSWERS", array(
    '0' => array('answers' => array(1, 2, 3, 0), 'result' => 0),
    '1' => array('answers' => array(1, 2, 3, 4), 'result' => 1),
    '2' => array('answers' => array(1, 2, 3, 4), 'result' => 2),
    '3' => array('answers' => array(2, 3, 4, 5), 'result' => 3),
    '4' => array('answers' => array(2, 3, 4, 5), 'result' => 4),
    '5' => array('answers' => array(2, 3, 4, 5), 'result' => 5),
    '6' => array('answers' => array(3, 4, 5, 6), 'result' => 6),
    '7' => array('answers' => array(4, 5, 6, 7), 'result' => 7),
    '8' => array('answers' => array(5, 6, 7, 8), 'result' => 8),
    '9' => array('answers' => array(6, 7, 8, 9), 'result' => 9),
));

define("LIMIT", 5);