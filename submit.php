<?php
require_once 'check.php';

if (!isset($_COOKIE['answer'])) {
    echo "Ban chua lam bai, <a href='questions.php'>Lam bai tai day</a>";
    exit();
} else {
    $selectedAnswers = json_decode($_COOKIE['answer'], true);
    $selectedAnswers = $_POST + $selectedAnswers;
    $score = checkQuestions($selectedAnswers);
    $result = '';
    if ($score > 7) {
        $result = "Score: " . $score . ", Sắp sửa làm được trợ giảng lớp PHP";
    } elseif ($score > 4 && $score < 7) {
        $result = "Score: " . $score . ", Cũng bình thường";
    } else {
        $result = "Score: " . $score . ", Bạn quá kém, cần ôn tập thêm";
    }
    setcookie("page", "", time() - 3600);
    setcookie("answer", "", time() - 3600);
}

function checkQuestions($selectedAnswers)
{
    $score = 0;
    foreach ($selectedAnswers as $keyQuestion => $userAnswer) {
        if (array_key_exists($keyQuestion, QUESTIONS)) {
            if (ANSWERS[$keyQuestion]['result'] == $userAnswer) {
                $score++;
            }
        }
    }
    return 10 / count(QUESTIONS) * $score;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Result</title>
</head>

<body style="text-align: center; margin-top: 300px;">
    <h1><?= $result ?></h1>
</body>

</html>
