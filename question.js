window.addEventListener('DOMContentLoaded', (event) => {
    let theme = document.querySelector('.theme');

    if (localStorage.getItem('data-theme')) {
        document.body.setAttribute('data-theme', localStorage.getItem('data-theme'))
    }
    theme.addEventListener('click', (e) => {
        let body = document.body;
        let themeType = body.getAttribute('data-theme') && (body.getAttribute('data-theme') ==
            'dark') ? 'light' : 'dark';
        theme.innerHTML = body.getAttribute('data-theme');
        body.setAttribute('data-theme', themeType);
        localStorage.setItem('data-theme', themeType);
    })
});